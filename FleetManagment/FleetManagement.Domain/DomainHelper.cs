﻿using FleetManagement.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Domain
{
    public class DomainHelper
    {
        private TruckingDB tdb = new TruckingDB();
        /// <summary>
        /// get all locations
        /// </summary>
        /// <returns>full location list</returns>
        public IEnumerable<LocationDTO> RetrieveLocations()
        {
            IEnumerable<Location> list = tdb.RetrieveLocations();
            List<LocationDTO> ret = new List<LocationDTO>();
            foreach (var item in list)
            {
                LocationDTO l = new LocationDTO();
                l.LocationId = item.LocationId;
                l.TruckNumber = item.TruckNumber;
                l.UserName = item.UserName;
                l.Lat = item.Lat;
                l.Lon = item.Lon;
                l.DateCreated = item.DateCreated;
                l.Active = item.Active;
                ret.Add(l);
            }
            return ret;
        }
        /// <summary>
        /// get all jobs
        /// </summary>
        /// <returns>full job list</returns>
        public IEnumerable<JobDTO> RetrieveJobs()
        {
            IEnumerable<Job> list = tdb.RetrieveJobs();
            List<JobDTO> ret = new List<JobDTO>();
            foreach (var item in list)
            {
                JobDTO j = new JobDTO();
                j.JobId = item.JobId;
                j.UserName = item.UserName;
                j.SLat = item.SLat;
                j.SLon = item.SLon;
                j.ELat = item.ELat;
                j.ELon = item.ELon;
                j.DateCreated = item.DateCreated;
                j.Active = item.Active;
                ret.Add(j);
            }
            return ret;
        }
        /// <summary>
        /// get all users
        /// </summary>
        /// <returns>full user list</returns>
        public IEnumerable<InfoDTO> RetrieveUser()
        {
            IEnumerable<Info> list = tdb.RetrieveUsers();
            List<InfoDTO> ret = new List<InfoDTO>();
            foreach (var item in list)
            {
                InfoDTO i = new InfoDTO();
                i.UserName = item.UserName;
                i.Pass = item.Pass;
                i.Setting = item.Setting;
                i.FirstName = item.FirstName;
                i.LastName = item.LastName;
                i.DateCreated = item.DateCreated;
                i.Active = item.Active;
                ret.Add(i);
            }
            return ret;
        }
        /// <summary>
        /// get all trucks
        /// </summary>
        /// <returns>full truck list</returns>
        public IEnumerable<TruckDTO> RetrieveTrucks()
        {
            IEnumerable<Truck> list = tdb.RetrieveTrucks();
            List<TruckDTO> ret = new List<TruckDTO>();
            foreach (var item in list)
            {
                TruckDTO t = new TruckDTO();
                t.TruckNumber = item.TruckNumber;
                t.UserName = item.UserName;
                t.CLat = item.CLat;
                t.CLon = item.CLon;
                t.Fault = item.Fault;
                t.DateCreated = item.DateCreated;
                t.Active = item.Active;
                ret.Add(t);
            }
            return ret;
        }
        /// <summary>
        /// add user to db
        /// </summary>
        /// <param name="info"></param>
        /// <Required>
        /// info.UserName
        /// info.Pass
        /// info.Setting
        /// info.FirstName
        /// info.LastName
        /// </Required>
        /// <returns>true on success</returns>
        public bool newUser(InfoDTO info)
        {
            Trucking t = new Trucking();
            var ti = t.Info;
            ti.UserName = info.UserName;
            ti.Pass = info.Pass;
            ti.Setting = info.Setting;
            ti.FirstName = info.FirstName;
            ti.LastName = info.LastName;
            return tdb.newUser(t);
        }
        /// <summary>
        /// add job to db
        /// </summary>
        /// <param name="info"></param>
        /// <param name="job"></param>
        /// <Required>
        /// info.UserName
        /// job.Slat      (start Latitude)
        /// job.Slon      (start Longituce)
        /// job.Elat      (end Latitude)
        /// job.Elon      (end Longitude)
        /// </Required>
        /// <returns>true on success</returns>
        public bool newJob(JobDTO job)
        {
            Trucking t = new Trucking();
            var ti = t.Info;
            var tj = t.Job;
            ti.UserName = "No Driver";
            tj.SLat = job.SLat;
            tj.SLon = job.SLon;
            tj.ELat = job.ELat;
            tj.ELon = job.ELon;
            return tdb.newJob(t);
        }
        /// <summary>
        /// add truck to db
        /// </summary>
        /// <param name="truck"></param>
        /// <Required>
        /// truck.TruckNumber
        /// truck.CLat      (current Latitude)
        /// truck.CLon      (current Longitude)
        /// </Required>
        /// <returns>true on success</returns>
        public bool newTruck(TruckDTO truck)
        {
            Trucking t = new Trucking();
            var tt = t.Truck;
            tt.TruckNumber = truck.TruckNumber;
            tt.CLat = truck.CLat;
            tt.CLon = truck.CLon;
            return tdb.newTruck(t);
        }
        /// <summary>
        /// update truck location
        /// </summary>
        /// <param name="truck"></param>
        /// <Required>
        /// truck.TruckNumber
        /// truck.CLat      (Current Latitude)
        /// truck.CLon      (Current Longitude)
        /// </Required>
        /// <returns>true on success</returns>
        public bool updateLocation(TruckDTO truck)
        {
            Trucking t = new Trucking();
            var tt = t.Truck;
            tt.TruckNumber = truck.TruckNumber;
            tt.CLat = truck.CLat;
            tt.CLon = truck.CLon;
            return tdb.updateLocation(t);
        }
        /// <summary>
        /// deactivate job(Finished)
        /// </summary>
        /// <param name="info"></param>
        /// <Required>
        /// info.Username
        /// </Required>
        /// <returns>true on success</returns>
        public bool deactivateJob(JobDTO job)
        {
            Trucking t = new Trucking();
            var ti = t.Job;
            ti.JobId = job.JobId;
            return tdb.deactivateJob(t);
        }
        /// <summary>
        /// deactivate truck add fault
        /// </summary>
        /// <param name="truck"></param>
        /// <Required>
        /// truck.CLat     (Current Latitude)
        /// truck.CLon     (Current Longitude)
        /// truck.TruckNumber
        /// truck.Fault
        /// </Required>
        /// <returns>true on success</returns>
        public bool deactivateTruck(TruckDTO truck)
        {
            Trucking t = new Trucking();
            var tt = t.Truck;
            tt.TruckNumber = truck.TruckNumber;
            tt.Fault = "accident";
            return tdb.deactivateTruck(t);
        }
        /// <summary>
        /// reactivate truck remove fault
        /// </summary>
        /// <param name="truck"></param>
        /// <Required>
        /// truck.TruckNumber
        /// truck.CLat     (Current Latitude)
        /// truck.CLon     (Current Longitude)
        /// </Required>
        /// <returns>true on success</returns>
        public bool activateTruck(TruckDTO truck)
        {
            Trucking t = new Trucking();
            var tt = t.Truck;
            tt.TruckNumber = truck.TruckNumber;
            return tdb.activateTruck(t);
        }
        /// <summary>
        /// remove a driver from a truck
        /// </summary>
        /// <param name="truck"></param>
        /// <Required>
        /// truck.TruckNumber
        /// </Required>
        /// <returns>true on success</returns>
        public bool removeDriverToTruck(TruckDTO truck)
        {
            Trucking t = new Trucking();
            var tt = t.Truck;
            tt.TruckNumber = truck.TruckNumber;
            return tdb.removeDriverToTruck(t);
        }
        /// <summary>
        /// add a driver to a truck
        /// </summary>
        /// <param name="info"></param>
        /// <param name="truck"></param>
        /// <Required>
        /// info.UserName
        /// truck.TruckNumber
        /// </Required>
        /// <returns>true on success</returns>
        public bool addDriverToTruck(InfoDTO info, TruckDTO truck)
        {
            Trucking t = new Trucking();
            var tt = t.Truck;
            var ti = t.Info;
            ti.UserName = info.UserName;
            tt.TruckNumber = truck.TruckNumber;
            tdb.activateTruck(t);
            return tdb.addDriverToTruck(t);
        }
        public bool deactivateEmployee(InfoDTO info)
        {
            Trucking t = new Trucking();
            t.Info.UserName = info.UserName;
            return tdb.deactivateEmployee(t);
        }
        public bool changeStatus(InfoDTO info)
        {
            Trucking t = new Trucking();
            t.Info.UserName = info.UserName;
            t.Info.Setting = info.Setting;
            return tdb.changeStatus(t);

        }
        public bool addToJob(InfoDTO info, JobDTO job)
        {
            Trucking t = new Trucking();
            t.Info.UserName = info.UserName;
            t.Job.JobId = job.JobId;
            return tdb.addToJob(t);
        }
    }
}