﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.Domain
{
    public class JobDTO
    {
        public int JobId { get; set; }

        public string UserName { get; set; }

        public decimal SLat { get; set; }

        public decimal SLon { get; set; }

        public decimal ELat { get; set; }

        public decimal ELon { get; set; }

        public System.DateTime DateCreated { get; set; }

        public bool Active { get; set; }

    }
}
