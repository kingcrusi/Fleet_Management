﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetManagement.Web2.Models
{
    public class LoginModel
    {
        public string UserName { get; set; }

        public string Pass { get; set; }

        public int Setting { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public System.DateTime DateCreated { get; set; }

        public bool Active { get; set; }

        public LoginModel() { }
    }
}