﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetManagement.Web2.Models
{
    public class EJobModel
    {
        public int TruckNumber { get; set; }

        public string UserName { get; set; }

        public decimal CLat { get; set; }

        public decimal CLon { get; set; }

        public string Fault { get; set; }

        public string Pass { get; set; }

        public int Setting { get; set; }

        public string FirstName { get; set; }

        public int JobId { get; set; }

        public decimal SLat { get; set; }

        public decimal SLon { get; set; }

        public decimal ELat { get; set; }

        public decimal ELon { get; set; }

        public string LastName { get; set; }
    }
}