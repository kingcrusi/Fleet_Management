﻿using FleetManagement.Web2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FleetManagement.Web2.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        ClientHelper ch = new ClientHelper();
        public ActionResult Employee()
        {
            return View("User");
        }
        public string GetUserListInfo()
        {
            var trucks = ch.RetrieveTrucks();
            var users = ch.RetrieveInfo();
            var job = ch.RetrieveJobs();
            List<Info> truck = new List<Info>();
            foreach (var u in users)
            {
                bool a = true;
                if (u.UserName.Equals("No Driver"))
                {
                    a = false;
                }
                foreach (var i in trucks)
                {
                    foreach (var j in job)
                    {
                        if (a)
                        {
                            if ((u.UserName.Equals(i.UserName)) && (u.UserName.Equals(j.UserName)))
                            {
                                if (u.Setting == 0)
                                {
                                    truck.Add(new Web2.Info() { truckNumber2 = i.TruckNumber.ToString(), UserName = i.UserName, Active = i.Active, FirstName = u.FirstName, LastName = u.LastName, Status = "Employee", Color = "table-success", Job2 = j.JobId.ToString() });
                                    a = false;
                                }
                                else if (u.Setting == 1)
                                {
                                    truck.Add(new Web2.Info() { truckNumber2 = i.TruckNumber.ToString(), UserName = i.UserName, Active = i.Active, FirstName = u.FirstName, LastName = u.LastName, Status = "Maintenance", Color = "table-danger", Job2 = j.JobId.ToString() });
                                    a = false;
                                }
                                else if (u.Setting == 2)
                                {
                                    truck.Add(new Web2.Info() { truckNumber2 = i.TruckNumber.ToString(), UserName = i.UserName, Active = i.Active, FirstName = u.FirstName, LastName = u.LastName, Status = "Management", Color = "table-warning", Job2 = j.JobId.ToString() });
                                    a = false;
                                }
                            //}else if((u.UserName.Equals(i.UserName)) && (u.UserName.Equals(j.UserName))){
                            //    if(j.Active == true)
                            //    {
                            //        if (u.Setting == 0)
                            //        {
                            //            truck.Add(new Web2.Info() { truckNumber2 = "No Truck Assigned", UserName = i.UserName, Active = i.Active, FirstName = u.FirstName, LastName = u.LastName, Status = "Employee", Color = "table-success", Job2 = j.JobId.ToString() });
                            //            a = false;
                            //        }
                            //        else if (u.Setting == 1)
                            //        {
                            //            truck.Add(new Web2.Info() { truckNumber2 = "No Truck Assigned", UserName = i.UserName, Active = i.Active, FirstName = u.FirstName, LastName = u.LastName, Status = "Maintenance", Color = "table-danger", Job2 = j.JobId.ToString() });
                            //            a = false;
                            //        }
                            //        else if (u.Setting == 2)
                            //        {
                            //            truck.Add(new Web2.Info() { truckNumber2 = "No Truck Assigned", UserName = i.UserName, Active = i.Active, FirstName = u.FirstName, LastName = u.LastName, Status = "Management", Color = "table-warning", Job2 = j.JobId.ToString() });
                            //            a = false;
                            //        }
                            //    }
                            //    else if(i.Active == true)
                            //    {
                            //        if (u.Setting == 0)
                            //        {
                            //            truck.Add(new Web2.Info() { truckNumber2 = i.TruckNumber.ToString(), UserName = i.UserName, Active = i.Active, FirstName = u.FirstName, LastName = u.LastName, Status = "Employee", Color = "table-success", Job2 = "No Job Assigned" });
                            //            a = false;
                            //        }
                            //        else if (u.Setting == 1)
                            //        {
                            //            truck.Add(new Web2.Info() { truckNumber2 = i.TruckNumber.ToString(), UserName = i.UserName, Active = i.Active, FirstName = u.FirstName, LastName = u.LastName, Status = "Maintenance", Color = "table-danger", Job2 = "No Job Assigned" });
                            //            a = false;
                            //        }
                            //        else if (u.Setting == 2)
                            //        {
                            //            truck.Add(new Web2.Info() { truckNumber2 = i.TruckNumber.ToString(), UserName = i.UserName, Active = i.Active, FirstName = u.FirstName, LastName = u.LastName, Status = "Management", Color = "table-warning", Job2 = "No Job Assigned" });
                            //            a = false;
                            //        }
                            //    }
                            }
                        }
                    }
                }
                if (a)
                {
                    if (u.Setting == 0)
                    {
                        truck.Add(new Web2.Info() { truckNumber2 = "No Truck Assigned",UserName = u.UserName, FirstName = u.FirstName, LastName=u.LastName ,Status = "Employee", Active = u.Active ,Color= "table-success" , Job2 = "No Job Assigned" });
                    }
                    else if(u.Setting == 1)
                    {
                        truck.Add(new Web2.Info() { truckNumber2 = "No Truck Assigned", UserName = u.UserName, FirstName = u.FirstName, LastName = u.LastName, Status = "Mainetenace", Active = u.Active ,Color= "table-danger", Job2 = "No Job Assigned" });
                    }
                    else
                    {
                        truck.Add(new Web2.Info() { truckNumber2 = "No Truck Assigned", UserName = u.UserName, FirstName = u.FirstName, LastName = u.LastName, Status = "Management", Active = u.Active ,Color= "table-warning", Job2 = "No Job Assigned" });
                    }
                }
            }
            return truck.ToJSON();
        }
        public string deleted(UserModel u)
        {
            var temp = u.UserName;
            Info i = new Info();
            i.UserName = temp;
            ch.deactivateEmployee(i);
            return "true";

        }
        public string saveChange(UserModel u)
        {
            var temp = u.UserName.ToString().Split(',');
            Info i = new Info();
            i.UserName = temp[0];
            i.Setting = int.Parse(temp[1]);
            ch.changeStatus(i);
            return "true";
        }
    }
}