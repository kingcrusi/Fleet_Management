﻿using FleetManagement.Web2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FleetManagement.Web2.Controllers
{
    public class TruckController : Controller
    {
        ClientHelper ch = new ClientHelper();
        // GET: Truck
        public ActionResult Truck()
        {
            var t = ch.RetrieveInfo();
            return View("Truck");
        }
        public string GetUserListInfo()
        {
            var users = ch.RetrieveInfo();
            List<Info> info = new List<Info>();
            foreach (var item in users)
            {
                info.Add(new Web2.Info(){ UserName = item.UserName});
            }
            return info.ToJSON();
        }
        public string GetTruckListInfo()
        {
            
            var trucks = ch.RetrieveTrucks();
            var users = ch.RetrieveInfo();
            var job = ch.RetrieveJobs();
            string holder = "";
            List<Truck> truck = new List<Truck> ();
            //truck.Add(new Web2.Truck() { TruckNumber = i.TruckNumber, UserName = i.UserName, CLat = i.CLat, CLon = i.CLon, Active = i.Active, Fault = "", FirstName = u.FirstName, LastName = u.LastName, Status = "Employee", Color = "table-warning" });
            foreach (var i in trucks)
            {
                bool a = true;
                foreach (var u in users)
                {
                    if (a)
                    {
                        if ((i.UserName.Equals(u.UserName)))
                        {
                            if (i.Active == true)
                            {
                                if(i.UserName == "No Driver")
                                {
                                    truck.Add(new Web2.Truck() { TruckNumber = i.TruckNumber, UserName = i.UserName, CLat = i.CLat, CLon = i.CLon, Active = i.Active, Fault = "", FirstName = u.FirstName, LastName = u.LastName, Status = "N/A", Color = "table-warning" });
                                    a = false;
                                }
                                else if(u.Setting == 0)
                                {
                                    truck.Add(new Web2.Truck() { TruckNumber = i.TruckNumber, UserName = i.UserName, CLat = i.CLat, CLon = i.CLon, Active = i.Active, Fault = "", FirstName = u.FirstName, LastName = u.LastName, Status = "Employee", Color = "table-success" });
                                    a = false;
                                }
                                else if (u.Setting == 1)
                                {
                                    truck.Add(new Web2.Truck() { TruckNumber = i.TruckNumber, UserName = i.UserName, CLat = i.CLat, CLon = i.CLon, Active = i.Active, Fault = "", FirstName = u.FirstName, LastName = u.LastName, Status = "Maintenance", Color = "table-success" });
                                    a = false;
                                }
                                else
                                {
                                    truck.Add(new Web2.Truck() { TruckNumber = i.TruckNumber, UserName = i.UserName, CLat = i.CLat, CLon = i.CLon, Active = i.Active, Fault = "", FirstName = u.FirstName, LastName = u.LastName, Status = "Management", Color = "table-success" });
                                    a = false;
                                }

                             }
                            else
                            {
                                truck.Add(new Web2.Truck() { TruckNumber = i.TruckNumber, UserName = i.UserName, CLat = i.CLat, CLon = i.CLon, Active = i.Active, Fault = i.Fault, FirstName = u.FirstName, LastName = u.LastName, Status = "N/A", Color = "table-danger" });
                                a = false;
                            }
                        }
                    }
                }
                if(a)
                {
                    truck.Add(new Web2.Truck() { TruckNumber = i.TruckNumber, UserName = "No Driver", CLat = i.CLat, CLon = i.CLon, Active = i.Active, Fault = "", FirstName = "N/A", LastName = "N/A", Status = "N/A", Color = "table-warning" });
                }
            }
            Session["TL"] = holder;
            return truck.ToJSON();
        }

        public string add(TruckModel t)
        {
            var temp = t.UserName.ToString().Split(',');
            Truck tr = new Web2.Truck();
            tr.TruckNumber = int.Parse(temp[0]);
            tr.CLat = decimal.Parse(temp[1]);
            tr.CLon = decimal.Parse(temp[2]);
            var pass = ch.newTruck(tr);
            return "true";
        }
        public string saveChange(TruckModel t)
        {
            var a = t.UserName.ToString().Split(',');
            Info i = new Info();
            Truck tr = new Web2.Truck();
            i.UserName = a[1];
            tr.TruckNumber = int.Parse(a[0]) ;
            ch.addDriverToTruck(i, tr);

            return "true";
        }
        public string remove(TruckModel t)
        {
            var a = t.UserName;
            Truck tr = new Truck();
            tr.TruckNumber = int.Parse(a);
            ch.removeDriverToTruck(tr);
            return "true";
        }
        public string deleted(TruckModel t)
        {
            var a = t.UserName;
            Truck tr = new Truck();
            tr.TruckNumber = int.Parse(a);
            ch.removeDriverToTruck(tr);
            ch.deactivateTruck(tr);
            return "true";
        }




    }
}