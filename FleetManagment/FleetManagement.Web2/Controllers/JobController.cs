﻿using FleetManagement.Web2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FleetManagement.Web2.Controllers
{
    public class JobController : Controller
    {
        // GET: Job
        ClientHelper ch = new ClientHelper();
        public ActionResult Job()
        {
            var info = ch.RetrieveInfo();
            var job = ch.RetrieveJobs();
            var j = Session["UN"];
            var jobAvailable = false;
            foreach (var item in job)
            {
                if (item.UserName == j.ToString() && item.Active == true)
                {
                    Session["SLAT"] = item.SLat;
                    Session["SLON"] = item.SLon;
                    Session["ELAT"] = item.ELat;
                    Session["ELON"] = item.ELon;
                    jobAvailable = true;
                }
            }
            if (jobAvailable)
            {
                return View("CheckJob");
            }else
            {
                return View("NoJob");
            }
           
        }
        public ActionResult CheckIn()
        {
            var truck = ch.RetrieveTrucks();
            var isTruck = false;
            foreach (var item in truck)
            {
                if (item.UserName == Session["UN"].ToString())
                {
                    isTruck = true;
                }
            }
            if (isTruck)
            {
                return View("CheckIn");
            }else
            {
                return View("NoTruck");
            }
            
        }
        public string NewLoc(JobModel j)
        {
            var loc = j.UserName.ToString().Split(',');
            Truck l = new Truck();
            l.CLat = decimal.Parse(loc[0]);
            l.CLon = decimal.Parse(loc[1]);
            l.TruckNumber = int.Parse(Session["TN"].ToString());
            var res = ch.updateLocation(l);
            return res.ToString();


        }
        
    }
}