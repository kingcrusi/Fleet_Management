﻿using FleetManagement.Web2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FleetManagement.Web2.Controllers
{
    public class EJobController : Controller
    {
        // GET: EJob
        ClientHelper ch = new ClientHelper();
        public ActionResult Job()
        {
            return View("Job");
        }
        public string GetJobListInfo()
        {
            var trucks = ch.RetrieveTrucks();
            var users = ch.RetrieveInfo();
            var job = ch.RetrieveJobs();
            List<Job> truck = new List<Job>();
            foreach (var j in job)
            {
                bool a = true;
                foreach (var u in users)
                {
                    foreach (var i in trucks)
                    {
                        if (a)
                        {
                            if ((j.UserName.Equals(u.UserName)) && (j.UserName.Equals(i.UserName)))
                            {

                                if (j.Active)
                                {
                                    if (i.UserName.Equals("No Driver"))
                                    {
                                        truck.Add(new Web2.Job() { JobId = j.JobId, UserName = j.UserName, FirstName = "Needs", LastName = "Driver", SLat = j.SLat, SLon = j.SLon, ELat = j.ELat, ELon = j.ELon, Active = j.Active, Color = "table-danger" });
                                        a = false;
                                    }else
                                    {
                                        truck.Add(new Web2.Job() { JobId = j.JobId, UserName = j.UserName, FirstName = u.FirstName, LastName = u.LastName, SLat = j.SLat, SLon = j.SLon, ELat = j.ELat, ELon = j.ELon, Active = j.Active, Color = "table-info" });
                                        a = false;
                                    }
                                }
                                else
                                {
                                    truck.Add(new Web2.Job() { JobId = j.JobId, UserName = j.UserName, FirstName = "Job", LastName = "complete", SLat = j.SLat, SLon = j.SLon, ELat = j.ELat, ELon = j.ELon, Active = j.Active, Color = "table-warning" });
                                    a = false;
                                }
                            }
                            
                        }
                    }
                }
                if (a)
                {
                    if (j.Active)
                    {
                        truck.Add(new Web2.Job() { JobId = j.JobId, UserName = j.UserName, FirstName = "Needs", LastName = "Truck", SLat = j.SLat, SLon = j.SLon, ELat = j.ELat, ELon = j.ELon, Active = j.Active, Color = "table-danger" });
                    }
                    else
                    {
                        truck.Add(new Web2.Job() { JobId = j.JobId, UserName = j.UserName, FirstName = "Job", LastName = "complete", SLat = j.SLat, SLon = j.SLon, ELat = j.ELat, ELon = j.ELon, Active = j.Active, Color = "table-warning" });
                    }
                    
                }
            }
            return truck.ToJSON();
        }
        public string newJob(JobModel t)
        {
            var temp = t.UserName.ToString().Split(',');
            Job j = new Web2.Job();
            Info i = new Info();
            i.UserName = "No Driver";
            j.SLat = decimal.Parse(temp[0]);
            j.SLon = decimal.Parse(temp[1]);
            j.ELat = decimal.Parse(temp[2]);
            j.ELon = decimal.Parse(temp[3]);
            var pass = ch.newJob(j);
            return pass.ToString();
        }
        public string complete(JobModel j)
        {
            var a = j.UserName;
            Job i = new Job();
            i.JobId = int.Parse(a);
            ch.dectivateJob(i);
            return "true";
        }
        public string GetUserListInfo()
        {
            var users = ch.RetrieveInfo();
            List<Info> info = new List<Info>();
            foreach (var item in users)
            {
                info.Add(new Web2.Info() { UserName = item.UserName });
            }
            return info.ToJSON();
        }
        public string saveChange(TruckModel t)
        {
            var a = t.UserName.ToString().Split(',');
            Info i = new Info();
            Job j = new Web2.Job();
            i.UserName = a[1];
            j.JobId = int.Parse(a[0]);
            ch.addToJob(i, j);

            return "true";
        }
    }
}