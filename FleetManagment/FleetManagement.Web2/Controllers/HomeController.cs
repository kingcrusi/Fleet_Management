﻿using FleetManagement.Web2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FleetManagement.Web2.Controllers
{
    public class HomeController : Controller
    {
        ClientHelper ch = new ClientHelper();
        
        public ActionResult Index()
        {
            ViewBag.Title = "FM USA";

            return View("Index");
        }
        public ActionResult Home()
        {
            ViewBag.Title = "FM USA";
            var user = ch.RetrieveInfo();
            var truck = ch.RetrieveTrucks();
            foreach(var item in user)
            {
                if(item.UserName == Session["UN"].ToString())
                {
                    Session["Setting"] = item.Setting;
                }
            }
            foreach(var item2 in truck)
            {
                if (item2.UserName == Session["UN"].ToString())
                {
                        Session["TN"] = item2.TruckNumber;
                }
            }

            if (Session["Setting"].ToString() == "0")
            {
                return View("Home");
            }else
            {
                return View("HomeEmployee");
            }
            
        }
        public ActionResult Register()
        {
            ViewBag.Title = "FM USA";

            return View("Register");
        }
        public string Verify(LoginModel x)
        {
            List<Info> i = new List<Info>();
            var user = ch.RetrieveInfo();
            var correct = false;
            var temp = x.UserName.Split(',');
            foreach (var item in user)
            {
                if (item.UserName.ToLower().Equals(temp[0].ToLower()))
                {
                    if (item.Pass.Equals(temp[1]))
                    {
                        Session["UN"] = temp[0].ToLower();
                        Session["FN"] = item.FirstName;
                        Session["LN"] = item.LastName;
                        correct = true;
                    }
                }
            }
            if (correct)
            {
                i.Add(new Web2.Info() {UserName="true" });
            }
            else { i.Add(new Web2.Info() { UserName = "false" }); }
            return i.ToJSON();
        }
        public string username(LoginModel u)
        {
            bool checker = true;
            var info = ch.RetrieveInfo();
            foreach (var item in info)
            {
                if (u.UserName.ToLower() == item.UserName.ToLower())
                {
                    checker = false;
                }
            }
            List<Info> i = new List<Info>();
            i.Add(new Info() { UserName = checker.ToString() });
            return i.ToJSON();
        }
        public string newUser(LoginModel u)
        {
            var a = u.UserName.ToString().Split(',');
            Info i = new Info();
            i.FirstName = a[0];
            i.LastName = a[1];
            i.UserName = a[2].ToLower();
            i.Pass = a[3];
            ch.newUser(i);
            return "true";
        }

    }
}
