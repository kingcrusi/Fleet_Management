﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.DataAccess
{
    public class Trucking
    {
        public Info Info { get; set; }
        
        public Truck Truck { get; set; }

        public Location Location { get; set; }

        public Job Job { get; set; }


        public Trucking()
        {
            Info = new Info();
            Truck = new Truck();
            Location = new Location();
            Job = new Job();
        }
    }
}
