﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagement.DataAccess
{
    public class TruckingDB
    {
        private SQLFMDBEntities db = new SQLFMDBEntities();

        public IEnumerable<Location> RetrieveLocations()
        {
            return db.Locations.ToList();
        }
        public IEnumerable<Job> RetrieveJobs()
        {
            return db.Jobs.ToList();
        }
        public IEnumerable<Info> RetrieveUsers()
        {
            return db.Infoes.ToList();
        }
        public IEnumerable<Truck> RetrieveTrucks()
        {
            return db.Trucks.ToList();
        }
        public bool newUser(Trucking truck)
        {
            var time = DateTime.UtcNow;
            truck.Info.DateCreated = time;
            truck.Info.Active = true;
            db.Infoes.Add(truck.Info);
            db.SaveChanges();
            return true;
        }
        public bool newJob(Trucking truck)
        {
            var time = DateTime.UtcNow;
            truck.Job.DateCreated = time;
            truck.Job.Active = true;
            truck.Job.UserName = truck.Info.UserName;
            db.Jobs.Add(truck.Job);
            db.SaveChanges();
            return true;
        }
        public bool newLocation(Trucking truck)
        {
            var time = DateTime.UtcNow;
            truck.Location.DateCreated = time;
            truck.Location.Active = true;
            truck.Location.UserName = truck.Info.UserName;
            truck.Location.TruckNumber = truck.Truck.TruckNumber;
            truck.Location.Lat = truck.Truck.CLat;
            truck.Location.Lon = truck.Truck.CLon;
            db.Locations.Add(truck.Location);
            db.SaveChanges();
            return true;
        }
        public bool newTruck(Trucking truck)
        {
            var time = DateTime.UtcNow;
            truck.Truck.DateCreated = time;
            truck.Truck.Active = true;
            truck.Truck.UserName = "No Driver";
            db.Trucks.Add(truck.Truck);
            db.SaveChanges();
            return true;
        }
        public bool updateLocation(Trucking truck)
        {
            var Id = db.Trucks.Find(truck.Truck.TruckNumber);
            var truckNumber = truck.Truck.TruckNumber;
            var truckClat = truck.Truck.CLat;
            var truckClon = truck.Truck.CLon;
            Trucking T = new Trucking();
            T.Truck.TruckNumber = Id.TruckNumber;
            T.Info.UserName = Id.UserName;
            T.Truck.CLat = Id.CLat;
            T.Truck.CLon = Id.CLon;
            newLocation(T);
            Id.CLat = truck.Truck.CLat;
            Id.CLon = truck.Truck.CLon;
            db.SaveChanges();
            return true;
        }
        public bool deactivateJob(Trucking truck)
        {
            var loc = db.Jobs.Find(truck.Job.JobId);
            loc.Active = false;
            db.SaveChanges();
            return true;
        }
        public bool deactivateTruck(Trucking truck)
        {
            var loc = db.Trucks.Find(truck.Truck.TruckNumber);
            loc.Active = false;
            loc.Fault = truck.Truck.Fault;
            loc.UserName = "No Driver";
            db.SaveChanges();
            return true;
        }
        public bool activateTruck(Trucking truck)
        {
            var loc = db.Trucks.Find(truck.Truck.TruckNumber);
            loc.Active = true;
            loc.Fault = "";
            db.SaveChanges();
            return true;
        }
        public bool removeDriverToTruck(Trucking truck)
        {
            var loc = db.Trucks.Find(truck.Truck.TruckNumber);
            loc.UserName = "No Driver";
            db.SaveChanges();
            return true;
        }
        public bool addDriverToTruck(Trucking truck)
        {
            var loc = db.Trucks.Find(truck.Truck.TruckNumber);
            loc.UserName = truck.Info.UserName;
            db.SaveChanges();
            return true;
        }
        public bool deactivateEmployee(Trucking truck)
        {
            var id = db.Trucks.FirstOrDefault(w => w.UserName.Equals(truck.Info.UserName)).TruckNumber;
            var jid = db.Jobs.FirstOrDefault(w => w.UserName.Equals(truck.Info.UserName)).JobId;
            Trucking t1 = new Trucking();
            Trucking t2 = new Trucking();
            t1.Job.JobId = jid;
            t2.Truck.TruckNumber = id;
            deactivateJob(t1);
            removeDriverToTruck(t2);
            var loc = db.Infoes.Find(truck.Info.UserName);
            loc.Active = false;
            db.SaveChanges();
            return true;
        }
        public bool changeStatus(Trucking truck)
        {
            var id = db.Infoes.Find(truck.Info.UserName);
            id.Setting = truck.Info.Setting;
            db.SaveChanges();
            return true;
        }
        public bool addToJob(Trucking truck)
        {
            var id = db.Jobs.Find(truck.Job.JobId);
            id.UserName = truck.Info.UserName;
            db.SaveChanges();
            return true;
        }
    }
}