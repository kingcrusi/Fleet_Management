
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/06/2016 13:23:31
-- Generated from EDMX file: C:\Revature\Fleet_Management\FleetManagment\FleetManagement.DataAccess\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [SQLFMDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[FMDB].[fk_Location_TruckNumber]', 'F') IS NOT NULL
    ALTER TABLE [FMDB].[Location] DROP CONSTRAINT [fk_Location_TruckNumber];
GO
IF OBJECT_ID(N'[FMDB].[fk_User_Job]', 'F') IS NOT NULL
    ALTER TABLE [FMDB].[Job] DROP CONSTRAINT [fk_User_Job];
GO
IF OBJECT_ID(N'[FMDB].[fk_User_Location]', 'F') IS NOT NULL
    ALTER TABLE [FMDB].[Location] DROP CONSTRAINT [fk_User_Location];
GO
IF OBJECT_ID(N'[FMDB].[fk_User_Truck]', 'F') IS NOT NULL
    ALTER TABLE [FMDB].[Truck] DROP CONSTRAINT [fk_User_Truck];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[FMDB].[Info]', 'U') IS NOT NULL
    DROP TABLE [FMDB].[Info];
GO
IF OBJECT_ID(N'[FMDB].[Job]', 'U') IS NOT NULL
    DROP TABLE [FMDB].[Job];
GO
IF OBJECT_ID(N'[FMDB].[Location]', 'U') IS NOT NULL
    DROP TABLE [FMDB].[Location];
GO
IF OBJECT_ID(N'[FMDB].[Truck]', 'U') IS NOT NULL
    DROP TABLE [FMDB].[Truck];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Trucks'
CREATE TABLE [dbo].[Trucks] (
    [TruckNumber] int  NOT NULL,
    [UserName] nvarchar(50)  NOT NULL,
    [CLat] decimal(10,6)  NOT NULL,
    [CLon] decimal(10,6)  NOT NULL,
    [DateCreated] datetime  NOT NULL,
    [Active] bit  NOT NULL,
    [Fault] nvarchar(max)  NULL
);
GO

-- Creating table 'Infoes'
CREATE TABLE [dbo].[Infoes] (
    [UserName] nvarchar(50)  NOT NULL,
    [Pass] nvarchar(50)  NOT NULL,
    [Setting] int  NOT NULL,
    [FirstName] nvarchar(50)  NOT NULL,
    [LastName] nvarchar(50)  NOT NULL,
    [DateCreated] datetime  NOT NULL,
    [Active] bit  NOT NULL
);
GO

-- Creating table 'Locations'
CREATE TABLE [dbo].[Locations] (
    [LocationId] int IDENTITY(1,1) NOT NULL,
    [TruckNumber] int  NOT NULL,
    [UserName] nvarchar(50)  NOT NULL,
    [Lat] decimal(10,6)  NOT NULL,
    [Lon] decimal(10,6)  NOT NULL,
    [DateCreated] datetime  NOT NULL,
    [Active] bit  NOT NULL
);
GO

-- Creating table 'Jobs'
CREATE TABLE [dbo].[Jobs] (
    [JobId] int IDENTITY(1,1) NOT NULL,
    [UserName] nvarchar(50)  NULL,
    [SLat] decimal(10,6)  NOT NULL,
    [SLon] decimal(10,6)  NOT NULL,
    [ELat] decimal(10,6)  NOT NULL,
    [ELon] decimal(10,6)  NOT NULL,
    [DateCreated] datetime  NOT NULL,
    [Active] bit  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [TruckNumber] in table 'Trucks'
ALTER TABLE [dbo].[Trucks]
ADD CONSTRAINT [PK_Trucks]
    PRIMARY KEY CLUSTERED ([TruckNumber] ASC);
GO

-- Creating primary key on [UserName] in table 'Infoes'
ALTER TABLE [dbo].[Infoes]
ADD CONSTRAINT [PK_Infoes]
    PRIMARY KEY CLUSTERED ([UserName] ASC);
GO

-- Creating primary key on [LocationId] in table 'Locations'
ALTER TABLE [dbo].[Locations]
ADD CONSTRAINT [PK_Locations]
    PRIMARY KEY CLUSTERED ([LocationId] ASC);
GO

-- Creating primary key on [JobId] in table 'Jobs'
ALTER TABLE [dbo].[Jobs]
ADD CONSTRAINT [PK_Jobs]
    PRIMARY KEY CLUSTERED ([JobId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UserName] in table 'Locations'
ALTER TABLE [dbo].[Locations]
ADD CONSTRAINT [fk_User_Location]
    FOREIGN KEY ([UserName])
    REFERENCES [dbo].[Infoes]
        ([UserName])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_User_Location'
CREATE INDEX [IX_fk_User_Location]
ON [dbo].[Locations]
    ([UserName]);
GO

-- Creating foreign key on [UserName] in table 'Trucks'
ALTER TABLE [dbo].[Trucks]
ADD CONSTRAINT [fk_User_Truck]
    FOREIGN KEY ([UserName])
    REFERENCES [dbo].[Infoes]
        ([UserName])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_User_Truck'
CREATE INDEX [IX_fk_User_Truck]
ON [dbo].[Trucks]
    ([UserName]);
GO

-- Creating foreign key on [TruckNumber] in table 'Locations'
ALTER TABLE [dbo].[Locations]
ADD CONSTRAINT [fk_Location_TruckNumber]
    FOREIGN KEY ([TruckNumber])
    REFERENCES [dbo].[Trucks]
        ([TruckNumber])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_Location_TruckNumber'
CREATE INDEX [IX_fk_Location_TruckNumber]
ON [dbo].[Locations]
    ([TruckNumber]);
GO

-- Creating foreign key on [UserName] in table 'Jobs'
ALTER TABLE [dbo].[Jobs]
ADD CONSTRAINT [fk_User_Job]
    FOREIGN KEY ([UserName])
    REFERENCES [dbo].[Infoes]
        ([UserName])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_User_Job'
CREATE INDEX [IX_fk_User_Job]
ON [dbo].[Jobs]
    ([UserName]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------