﻿using FleetManagement.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FleetManagement.Test
{
    public class TestDataAccess
    {
        private TruckingDB domain = new TruckingDB();
        [Fact]
        public void newJob_Test()
        {
            Trucking t = new Trucking();
            var i = t.Info;
            var j = t.Job;
            bool actual;
            var expected = true;
            i.UserName = "testUser";
            j.SLat = decimal.Parse((35.221997).ToString());
            j.SLon = decimal.Parse((-101.831297).ToString());
            j.ELat = decimal.Parse((35.221997).ToString());
            j.ELon = decimal.Parse((-101.831297).ToString());
            actual = domain.newJob(t);
            Assert.True(actual == expected);
        }
    }
}
