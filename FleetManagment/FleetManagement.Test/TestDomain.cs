﻿using FleetManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FleetManagement.Test
{

    public class TestDomain
    {
        private DomainHelper domain = new DomainHelper();
        [Fact]
        public void newUser_Test()
        {
            InfoDTO i = new InfoDTO();
            bool actual;
            var expected = true;
            i.UserName = "testUser";
            i.Pass = "testPass";
            i.Setting = 0;
            i.FirstName = "testFirst";
            i.LastName = "testLast";
            actual = domain.newUser(i);
            Assert.True(actual == expected);
        }
        [Fact]
        public void newJob_Test()
        {
            JobDTO j = new JobDTO();
            bool actual;
            var expected = true;
            j.SLat = decimal.Parse((35.221997).ToString());
            j.SLon = decimal.Parse((-101.831297).ToString());
            j.ELat = decimal.Parse((35.221997).ToString());
            j.ELon = decimal.Parse((-101.831297).ToString());
            actual = domain.newJob( j);
            Assert.True(actual == expected);
        }
        [Fact]
        public void updateLocation_Test()
        {
            TruckDTO t = new TruckDTO();
            bool actual;
            var expected = true;
            t.TruckNumber = 43;
            t.CLat = decimal.Parse((35.221997).ToString());
            t.CLon = decimal.Parse((-101.831297).ToString());
            actual = domain.updateLocation(t);
            Assert.True(actual == expected);
        }
        [Fact]
        public void newTruck_Test()
        {
            TruckDTO t = new TruckDTO();
            bool actual;
            var expected = true;
            t.TruckNumber = 35;
            t.CLat = decimal.Parse((35.221997).ToString());
            t.CLon = decimal.Parse((-101.831297).ToString());
            actual = domain.newTruck(t);
            Assert.True(actual == expected);
        }
        //[Fact]
        //public void deactivateJob_Test()
        //{
        //    InfoDTO i = new InfoDTO();
        //    i.UserName = "kingcrusi";
        //    bool actual;
        //    var expected = true;
        //    actual = domain.deactivateJob(i);
        //    Assert.True(actual == expected);
        //}
        [Fact]
        public void deactivateTruck_Test()
        {
            TruckDTO t = new TruckDTO();
            bool actual;
            var expected = true;
            t.TruckNumber = 0;
            t.Fault = "Truck sold";
            actual = domain.deactivateTruck(t);
            Assert.True(actual == expected);
        }
        [Fact]
        public void activateTruck_Test()
        {
            TruckDTO t = new TruckDTO();
            bool actual;
            var expected = true;
            t.TruckNumber = 3;
            t.CLat = decimal.Parse((35.221997).ToString());
            t.CLon = decimal.Parse((-101.831297).ToString());
            actual = domain.activateTruck(t);
            Assert.True(actual == expected);
        }
        [Fact]
        public void removeDriverToTruck_Test()
        {
            TruckDTO t = new TruckDTO();
            bool actual;
            var expected = true;
            t.TruckNumber = 43;
            actual = domain.removeDriverToTruck(t);
            Assert.True(actual == expected);
        }
        [Fact]
        public void addDriverToTruck_Test()
        {
            TruckDTO t = new TruckDTO();
            InfoDTO i = new InfoDTO();
            bool actual;
            var expected = true;
            i.UserName = "kingcrusi";
            t.TruckNumber = 43;
            actual = domain.addDriverToTruck(i,t);
            Assert.True(actual == expected);
        }

    }
}
