﻿using GeoJSON.Net.Feature;
using GeoJSON.Net.Geometry;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetManagement.Web.Models
{
    public class MapModel
    {
        public int TruckNumber { get; set; }

        public int DriverId { get; set; }

        public System.DateTime DateCreated { get; set; }

        public bool Active { get; set; }

        public List<Location> Locations{ get;  set;}

        public HtmlString gs { get; set; }

        public MapModel(IEnumerable<Location> x)
        {
            var model = new FeatureCollection();

            List<Location> ti = new List<Location>();
            foreach(var item in x)
            {
                Location t = new Location();
                t.Latitude = item.Latitude;
                t.Longitude = item.Longitude;
                t.TruckNumber = item.TruckNumber;
                t.DateCreated = item.DateCreated;
                ti.Add(t);

                var geom = new Point(new GeographicPosition(System.Convert.ToDouble(item.Latitude), System.Convert.ToDouble(item.Longitude)));
                var prop = new Dictionary<string, Object>
                {
                    {"Truck Number" , item.TruckNumber }
                };
                var feature = new GeoJSON.Net.Feature.Feature(geom, prop);
                model.Features.Add(feature);
                gs = new HtmlString(JsonConvert.SerializeObject(model));
    }
            Locations = ti;
        }


    }
}