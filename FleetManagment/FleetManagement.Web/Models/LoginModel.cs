﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.WebPages.Html;

namespace FleetManagement.Web.Models
{
    public class LoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<SelectListItem> Trucks { get; set; }
        public List<SelectListItem> Users { get; set; }
        public string chosenU { get; set; }
        public string chosenT { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }

        public LoginModel()
        {

        }
        public LoginModel(string x)
        {
            UserName = x;
        }
        public LoginModel(IEnumerable<Info> b,IEnumerable<Truck> c)
        {

            var listUser = new List<SelectListItem>();
            foreach(var item in c)
            {
                listUser.Add(new SelectListItem { Text = item.TruckNumber.ToString(), Value = item.TruckNumber.ToString() });
            }
            Trucks = listUser;

            var listTruck = new List<SelectListItem>();
            foreach(var item in b)
            {
                listTruck.Add(new SelectListItem { Text = item.FirstName.ToString(), Value = item.FirstName.ToString() });
            }
            Users = listTruck;
        }
    }

}