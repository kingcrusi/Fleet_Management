﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetManagement.Web
{
    public class Truck
    {
        public int TruckNumber { get; set; }

        public string UserName { get; set; }

        public decimal CLat { get; set; }

        public decimal CLon { get; set; }

        public string Fault { get; set; }

        public System.DateTime DateCreated { get; set; }

        public bool Active { get; set; }
    }
}