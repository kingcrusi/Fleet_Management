﻿using FleetManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetManagement.Web
{
    public class WebMapper
    {
        public static TruckDTO TruckMapper(Truck truck)
        {
            var t = new TruckDTO();
            t.TruckNumber = truck.TruckNumber;
            t.UserName = truck.UserName;
            t.CLat = truck.CLat;
            t.CLon = truck.CLon;
            t.DateCreated = truck.DateCreated;
            t.Active = truck.Active;
            return t;
        }
        public static InfoDTO InfoMapper(Info info)
        {
            var d = new InfoDTO();
            d.UserName = info.UserName;
            d.Pass = info.Pass;
            d.Setting = info.Setting;
            d.FirstName = info.FirstName;
            d.LastName = info.LastName;
            d.DateCreated = info.DateCreated;
            d.Active = info.Active;
            return d;
        }
        public static LocationDTO LocationMapper(Location location)
        {
            var l = new LocationDTO();
            l.LocationId = location.LocationId;
            l.TruckNumber = location.TruckNumber;
            l.UserName = location.UserName;
            l.Lat = location.Lat;
            l.Lon = location.Lon;
            l.DateCreated = location.DateCreated;
            l.Active = location.Active;
            return l;
        }
        public static JobDTO JobMapper(Job job)
        {
            var u = new JobDTO();
            u.JobId = job.JobId;
            u.UserName = job.UserName;
            u.SLat = job.SLat;
            u.SLon = job.SLon;
            u.ELat = job.ELat;
            u.ELon = job.ELon;
            u.DateCreated = job.DateCreated;
            u.Active = job.Active;
            return u;
        }
    }
}