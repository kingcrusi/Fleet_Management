﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetManagement.Web
{
    public class AllVar
    {
        public int Lat { get; set; }

        public int Lon { get; set; }

        public string Username { get; set; }

        public int Truck { get; set; }
    }
}