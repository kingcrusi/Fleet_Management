﻿using FleetManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetManagement.Web
{
    public class ClientHelper
    {
        private DomainHelper domain = new DomainHelper();
        public IEnumerable<Location> RetrieveLocations()
        {
            var location = domain.RetrieveLocations();
            List<Location> ret = new List<Location>();
            foreach (var item in location)
            {
                Location l = new Location();
                l.LocationId = item.LocationId;
                l.TruckNumber = item.TruckNumber;
                l.UserName = item.UserName;
                l.Lat = item.Lat;
                l.Lon = item.Lon;
                l.DateCreated = item.DateCreated;
                l.Active = item.Active;
                ret.Add(l);
            }
            return ret;
        }
        public IEnumerable<Info> RetrieveInfo()
        {
            var list = domain.RetrieveUser();
            List<Info> ret = new List<Info>();
            foreach (var item in list)
            {
                Info i = new Info();
                i.UserName = item.UserName;
                i.Pass = item.Pass;
                i.Setting = item.Setting;
                i.FirstName = item.FirstName;
                i.LastName = item.LastName;
                i.DateCreated = item.DateCreated;
                i.Active = item.Active;
                ret.Add(i);
            }
            return ret;
        }
        public IEnumerable<Job> RetrieveJobs()
        {
            var list = domain.RetrieveJobs();
            List<Job> ret = new List<Job>();
            foreach (var item in list)
            {
                Job j = new Job();
                j.JobId = item.JobId;
                j.UserName = item.UserName;
                j.SLat = item.SLat;
                j.SLon = item.SLon;
                j.ELat = item.ELat;
                j.ELon = item.ELon;
                j.DateCreated = item.DateCreated;
                j.Active = item.Active;
                ret.Add(j);
            }
            return ret;
        }
        public IEnumerable<Truck> RetrieveTrucks()
        {
            var list = domain.RetrieveTrucks();
            List<Truck> ret = new List<Truck>();
            foreach (var item in list)
            {
                Truck t = new Truck();
                t.TruckNumber = item.TruckNumber;
                t.UserName = item.UserName;
                t.CLat = item.CLat;
                t.CLon = item.CLon;
                t.DateCreated = item.DateCreated;
                t.Active = item.Active;
                ret.Add(t);
            }
            return ret;
        }
        public bool newUser(Info info)
        {
            var i = WebMapper.InfoMapper(info);
            return domain.newUser(i);
        }
        public bool newJob(Info info, Job job)
        {
            var i = WebMapper.InfoMapper(info);
            var j = WebMapper.JobMapper(job);
            return domain.newJob(i,j);
        }
        public bool newLocation(Info info, Truck truck, Location location)
        {
            var i = WebMapper.InfoMapper(info);
            var t = WebMapper.TruckMapper(truck);
            var l = WebMapper.LocationMapper(location);
            return domain.newLocation(i,t,l);
        }
        public bool newTruck(Truck truck)
        {
            var t = WebMapper.TruckMapper(truck);
            return domain.newTruck(t);
        }
        public bool updateLocation(Truck truck)
        {
            var t = WebMapper.TruckMapper(truck);
            return domain.newTruck(t);
        }
        public bool dectivateJob(Info info)
        {
            var i = WebMapper.InfoMapper(info);
            return domain.deactivateJob(i);
        }
        public bool deactivateTruck(Truck truck)
        {
            var t = WebMapper.TruckMapper(truck);
            return domain.deactivateTruck(t);
        }
        public bool activateTruck(Truck truck)
        {
            var t = WebMapper.TruckMapper(truck);
            return domain.activateTruck(t);
        }
        public bool removeDriverToTruck(Truck truck)
        {
            var t = WebMapper.TruckMapper(truck);
            return domain.removeDriverToTruck(t);
        }
        public bool addDriverToTruck(Info info, Truck truck)
        {
            var i = WebMapper.InfoMapper(info);
            var t = WebMapper.TruckMapper(truck);
            return domain.addDriverToTruck(i,t);
        }








    }
}