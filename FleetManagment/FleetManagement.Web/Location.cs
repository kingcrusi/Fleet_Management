﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetManagement.Web
{
    public class Location
    {
        public int LocationId { get; set; }

        public int TruckNumber { get; set; }

        public string UserName { get; set; }

        public decimal Lat { get; set; }

        public decimal Lon { get; set; }

        public System.DateTime DateCreated { get; set; }

        public bool Active { get; set; }
    }
}