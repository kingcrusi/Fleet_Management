﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FleetManagement.Web.Controllers
{
    public class LocationController : ApiController
    {
        ClientHelper h = new ClientHelper();
        [HttpGet]
        public HttpResponseMessage GetRetrieveAccount()
        {

            var javaScriptSerializer = new
            System.Web.Script.Serialization.JavaScriptSerializer();
            string jsonString = javaScriptSerializer.Serialize(h.GetAllTrucks());


            return Request.CreateResponse(HttpStatusCode.OK, jsonString);
        }
        [HttpPost]
        public HttpResponseMessage AddLocation([FromBody]AllVar a)
        {
            Location l = new Location();
            User u = new User();
            l.Latitude = a.Lat;
            l.Longitude = a.Lon;
            u.Username = a.Username;
            l.TruckNumber = a.Truck;
            return Request.CreateResponse<bool>(HttpStatusCode.OK, h.newLocation(u,l));
        }

    }
}
