﻿using FleetManagement.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace FleetManagement.Web.Controllers
{
    public class LoginController : Controller
    {
        ClientHelper ch = new ClientHelper();
        public ViewResult Login()
        {
            var user = ch.GetUserList();
            var truck = ch.GetAllTrucks();
            return View("Login", new LoginModel(user, truck));
        }
        public ViewResult NewLocation() { return View(); }
        public ViewResult Map() { return View("Select"); }
        public ViewResult Home() { return View("Home"); }
        public ViewResult NewCreation() { return View(); }
        public ViewResult select() {
            var user = ch.GetUserList();
            var truck = ch.GetAllTrucks();
            return View("Select", new LoginModel(user, truck));
        }
        public ViewResult Verify(LoginModel x)
        {
            
            var correct = false;
            var l = ch.GetUserList();
            foreach(var item in l)
            {
                if(item.Username == x.UserName)
                {
                    if(item.Password == x.Password)
                    {
                        correct = true;
                    }
                }
            }
            if (!correct)
            {
                return View("LoginFailure");
            }
            else
            {
                Session["TN"] = x.chosenT;
                Session["UN"] = x.UserName;
                return View("Home");
            }
        }
        public ViewResult ViewAll()
        {
            var res = ch.GetLocationList();

            return View("Map", new MapModel(res));
        }
        public ViewResult chooseU(LoginModel u)
        {
            var user = new Web.User();
            user.Username = u.chosenU;
            var res = ch.GetLocationListByUsername(user);
            return View("Map", new MapModel(res));
        }
        public ViewResult chooseT(LoginModel u)
        {
            var num = new Web.Truck();
            num.TruckNumber = Int32.Parse(u.chosenT);
            var res = ch.GetLocationListByTruckNumber(num);
            return View("Map", new MapModel(res));
        }
        public ViewResult checkin()
        {
            return View("Home");
        }
        [HttpPost]
        public ActionResult NewLoc(LoginModel av)
        {
            User user = new Web.User();
            Location l = new Location();
            user.Username = Session["UN"].ToString();
            l.TruckNumber = Int32.Parse(Session["TN"].ToString());
            var temp = av.lat.ToString().Split(',');
            l.Latitude = decimal.Parse(temp[0]);
            l.Longitude = decimal.Parse(temp[1]);
            var res = ch.newLocation(user, l);
            return RedirectToAction("Home");
        }
        public ViewResult confirmSignUp(LoginModel u)
        {
            User user = new Web.User();
            Driver driver = new Driver();
            user.Username = u.UserName;
            user.Password = u.Password;
            user.Email = u.Email;
            driver.FirstName = u.FirstName;
            driver.LastName = u.LastName;
            var res = ch.newUser(user, driver);
            var userq = ch.GetUserList();
            var truck = ch.GetAllTrucks();
            return View("Login", new LoginModel(userq, truck));
        }
        public ViewResult signup(){return View();}


    }
}
