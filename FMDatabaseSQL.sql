--use master;
--go

--drop database for quick run
-- comment out later
--drop database SQLFMDB;
--go
---------------------------------

--create database
--create database SQLFMDB;
--go

----select DB;
--use FleetManagementDB;
--go

-- create schema
--create schema FMDB;
--go


alter table FMDB.Location
drop constraint fk_Location_TruckNumber;


alter table FMDB.Location
drop constraint fk_User_Location;


alter table FMDB.Truck
drop constraint fk_User_Truck;

alter table FMDB.Job
drop constraint fk_User_Job;

drop table FMDB.Info;
drop table FMDB.Truck;
drop table FMDB.Location;
drop table FMDB.Job;





--create tabels for th FMDB schema
create table FMDB.Info
(
UserName nvarchar(50) not null,
Pass nvarchar(50) not null,
Setting int not null, 
FirstName nvarchar(50) not null,
LastName nvarchar(50) not null,
DateCreated datetime2(0) not null,
Active bit not null
)
create table FMDB.Truck
(
TruckNumber int not null,
UserName nvarchar(50) not null,
CLat decimal(10,6) not null,
CLon decimal(10,6) not null,
DateCreated datetime2(0) not null,
Active bit not null
)
create table FMDB.Location
(
LocationId int not null identity (1,1),
TruckNumber int not null,
UserName nvarchar(50) not null,
Lat decimal(10,6) not null,
Lon decimal(10,6) not null,
DateCreated datetime2(0) not null,
Active bit not null
)
create table FMDB.Job
(
JobId int not null identity(1,1),
UserName nvarchar(50),
SLat decimal(10,6) not null,
SLon decimal(10,6) not null,
ELat decimal(10,6) not null,
ELon decimal(10,6) not null,
DateCreated datetime2(0) not null,
Active bit not null
)


--set primary keys
alter table FMDB.Info
add constraint pk_Trucks_UserName primary key clustered(UserName);

alter table FMDB.Truck
add constraint pk_Locations_TruckNumber primary key clustered(TruckNumber);

alter table FMDB.Location
add constraint LocationId primary key clustered(LocationId);

alter table FMDB.Job
add constraint JobId primary key clustered(JobId);

--set foriegn keys
alter table FMDB.Job
add constraint fk_User_Job
foreign key (UserName) references FMDB.Info(UserName);

alter table FMDB.Location
add constraint fk_Location_TruckNumber
foreign key (TruckNumber) references FMDB.Truck(TruckNumber);

alter table FMDB.Location
add constraint fk_User_Location
foreign key (UserName) references FMDB.Info(UserName);

alter table FMDB.Truck
add constraint fk_User_Truck
foreign key (UserName) references FMDB.Info(Username);

--for testing
insert into FMDB.Info(UserName,Pass,Setting,FirstName,LastName,DateCreated,Active)
values('No Driver','abc123',0,'No','Driver',GETDATE(),1);

insert into FMDB.Info(UserName,Pass,Setting,FirstName,LastName,DateCreated,Active)
values('kingcrusi','abc123',2,'Cory','Davenport',GETDATE(),1);

insert into FMDB.Info(UserName,Pass,Setting,FirstName,LastName,DateCreated,Active)
values('jclarke3','abc123',1,'Jordan','Clarke',GETDATE(),1);

insert into FMDB.Info(UserName,Pass,Setting,FirstName,LastName,DateCreated,Active)
values('freddio','abc123',0,'Fred','Belotte',GETDATE(),1);

insert into FMDB.Truck(TruckNumber,UserName,CLat,CLon,DateCreated,Active)
values(43,'kingcrusi',38.953327,-77.350210,GETDATE(),1);

insert into FMDB.Truck(TruckNumber,UserName,CLat,CLon,DateCreated,Active)
values(3,'freddio',35.693327,-77.35021,GETDATE(),1);

insert into FMDB.Truck(TruckNumber,UserName,CLat,CLon,DateCreated,Active)
values(16,'jclarke3',33.15678,-117.147217,GETDATE(),1);

insert into FMDB.Job(UserName,SLat,SLon,ELat,ELon,DateCreated,Active)
values('freddio',35.2220,-101.8313,38.967017,-77.414389,GETDATE(),1);

insert into FMDB.Job(UserName,SLat,SLon,ELat,ELon,DateCreated,Active)
values('jclarke3',40.564063,-122.343812,31.840233,-116.575928,GETDATE(),1);

insert into FMDB.Job(UserName,SLat,SLon,ELat,ELon,DateCreated,Active)
values('kingcrusi',32.731841,-117.147217,38.967017,-77.414389,GETDATE(),1);

Alter Table FMDB.Truck
Add Fault nvarchar(MAX) null;


select * from FMDB.Info;

select * from FMDB.Truck;

select * from FMDB.Location;

select * from FMDB.Job;